﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace Test2
{

    public class Level : MonoBehaviour
    {
        #region private fields
        [Tooltip("Настройки уровня")]
        [SerializeField]LevelData _data;

        [Tooltip("Позиция защищаемого замка")]
        [SerializeField] Transform _castlePosition;
        [Tooltip("Отображение жизни замка")]
        [SerializeField] TextMeshProUGUI _castleHeathText;
        [Tooltip("Отображение монет")]
        [SerializeField] TextMeshProUGUI _coinsText;
        [Tooltip("Отображение количества вол")]
        [SerializeField] TextMeshProUGUI _wavesText;
        [Tooltip("Отображение текущая волна")]
        [SerializeField] TextMeshProUGUI _waveText;
        #endregion
        #region public methods, properties
        public LevelData Data
        {
            get
            {
                return _data;
            }
        }
        public Transform CastlePosition
        {
            get
            {
                return _castlePosition;
            }
        }

        public TextMeshProUGUI CastleHealhText
        {
            get
            {
                return _castleHeathText;
            }
        }
        public TextMeshProUGUI CoinsText
        {
            get
            {
                return _coinsText;
            }
        }
        public TextMeshProUGUI WavesText
        {
            get
            {
                return _wavesText;
            }
        }

        public TextMeshProUGUI WaveText
        {
            get
            {
                return _waveText;
            }
        }

        #endregion
    }
}
