﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Library
{
    /// <summary>
    ///  Статический класс 
    ///  В котом содержатся мат методы 
    /// </summary>
    public static class MatchBase
    {
        /// <summary>
        /// Метод возвращает угол на который нужно повернуть
        ///  обьект position что бы он был направлен на обьект target
        /// 
        /// </summary>
        /// <param name="position"> объект для которого нужно найти угол </param>
        /// <param name="forward"> направление "вперед"  обьекта position </param>
        /// <param name="target"> объект цель</param>
        /// <returns></returns>
        public static float angleLookRotation(Vector2 position, Vector2 forward, Vector2 target)
        {
            Vector2 tmpVec2 = target - position;
            float angle = Vector2.Angle(tmpVec2, forward);
            float sa = tmpVec2.x * forward.y - tmpVec2.y * forward.x;
            if (sa > 0) angle *= -1;
            return angle;
        }

        /// <summary>
        /// Метод возвращает угол на который нужно повернуть
        ///  обьект position что бы он был направлен на обьект target
        /// Обьект поворачивается со скоростью angleRotate
        /// </summary>
        /// <param name="position"> объект для которого нужно найти угол </param>
        /// <param name="forward"> направление "вперед"  обьекта position </param>
        /// <param name="target"> объект цель</param>
        /// <param name="angleRotate"> скорость поворота обьекта position</param>
        /// <returns></returns>
        public static float AngleRotateTowards(Vector2 position, Vector2 forward, Vector2 target, float angleRotate)
        {
            Vector2 tmpVec2 = target - position;
            float angle = Vector2.Angle(tmpVec2, forward);
            if (angle > angleRotate)
            {
                angle = angleRotate;
            }
            float sa = tmpVec2.x * forward.y - tmpVec2.y * forward.x;
            if (sa > 0) angle *= -1;
            return angle;
        }


    }
}
