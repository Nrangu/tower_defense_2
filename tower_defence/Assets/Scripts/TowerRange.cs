﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class TowerRange : MonoBehaviour
    {
        #region private fields
        [SerializeField] GameObject _towerRange;
//        SpriteRenderer _spriteRenderer = null;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            float range = gameObject.transform.parent.GetComponent<Tower>().Data.Range;
            _towerRange.SetActive(false);
            _towerRange.transform.localScale = new Vector3(range*2, range*2, 1);


        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnMouseEnter()
        {

            _towerRange.SetActive(true);
        }
        private void OnMouseExit()
        {
            _towerRange.SetActive(false);
        }
        #endregion
    }
}
