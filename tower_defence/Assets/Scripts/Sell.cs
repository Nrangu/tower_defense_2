﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Sell : MonoBehaviour
    {
        #region private fields
        [SerializeField] GameObject _parent;
        GameObject _menuSell;
        #endregion
        #region private methods

        private void OnMouseDown()
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("On button");
                return;
            }
            GetComponent<MenuTower2>().Init(_parent);

        }
        #endregion
    }
}
