﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Library;
using UnityEngine.SceneManagement;


namespace Test2
{
    public class Main : MonoBehaviour
    {
        #region private fields

        Level _level;
        float _health = 0;
        public int _coins = 0;
        int _Waves = 0;
        int _Wave = 0;
        static Main _ref;
        Test2 _test2;
        [SerializeField] GameObject _menuWinPrefab;
        GameObject _menuWin;
        int _currentScene = 0;
        int _countScenes = 2;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;

            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);

 



        }
        void Init()
        {
            _level =  GameObject.Find("Level").GetComponent<Level>();

            _test2 = Test2.Instace();
            _test2.Start();
            _test2.Main = this;
            _test2.Play();
            _test2.OnWin += OnWin;

            _health = _level.Data.CastleHealth;
            _coins = _level.Data.Coins;
            _level.CastleHealhText.text  = _health.ToString();
            _level.CoinsText .text = _coins.ToString();
            SpawnersManager.Inctance().Init();
            _menuWin = Instantiate(_menuWinPrefab);
            _menuWin.SetActive(false);
        }
        void OnWin()
        {
            _currentScene++;
            _menuWin.SetActive(true);
            
            _menuWin.GetComponent<Win>().OnWinClick += () => {
                                                                if (_currentScene == _countScenes)
                                                                {
                                                                    Application.Quit();
                                                                    return;
                    
                                                                };

                                                                SceneManager.LoadScene(_currentScene);
                                                            };
            

        }
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
        }
        private void Quit()
        {
            PlayerPrefs.Save();
        }
        private void OnApplicationQuit()
        {
            Quit();
        }
        private void OnApplicationPause(bool pause)
        {
            Quit();
        }
        private void OnEnable()
        {
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            Init();

        }

        #endregion
        #region public methods
        public void GamePlay()
        {
            _test2.Play();
        }
        #endregion
        #region public properties
        #endregion
        #region public methods
        public void Damage(float damage_)
        {
            _health -= damage_;
            if (_health <= 0)
            {
                _health = 0;
                Test2.Instace().GameOver();
            }
            _level.CastleHealhText.text = _health.ToString();
        }
        public Transform CastlePosition()
        {
            return _level.CastlePosition;
        }
        public int Coins
        {
            get
            {
                return _coins;
            }
        }
        public void ChangeCoins(int value_)
        {
            _coins += value_;
            if (_coins < 0)
            {
                _coins = 0;
            }
            _level.CoinsText.text = _coins.ToString();
        }
        public Level Level
        {
            get
            {
                return _level;
            }
        }
        public int Waves
        {
            get
            {
                return _Waves;
            }

            set
            {
                _Waves = value;
                _level.WavesText.text = _Waves.ToString();
            }
        }

        public int Wave
        {
            get
            {
                return _Wave;
            }
            set
            {
                _Wave = value;
                _level.WaveText.text = _Wave.ToString();
            }
        }
        #endregion

    }
}
