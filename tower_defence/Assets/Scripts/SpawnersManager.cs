﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Test2
{
    public class SpawnersManager : MonoBehaviour
    {
        #region private fields
        List<Spawner> _spawners = new List<Spawner>();
        [SerializeField] GameObject _delayBetweenWavesPrefab;
        DelayBetweenWaves _delayBetweenWaves;
        [SerializeField] int _countSeconds = 10;
        static SpawnersManager _ref = null;
        int _countWaves;
        int _currentWave = 0;
        #endregion
        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;

            }
            else
            {
                Destroy(gameObject);
            }

            //Init();
        }
        // Start is called before the first frame update
        void Start()
        {


        }

        // Update is called once per frame
        void Update()
        {
            if (!Test2.Instace().IsPlay) return;
            if( (_currentWave == _countWaves) && (IsSpawns() == false))
            {
                Test2.Instace().Win();
                return;
                //SceneManager.LoadScene(1);
            }
            if ((IsSpawns() == false) && (_delayBetweenWaves.Active == false))
            {
                _delayBetweenWaves.ResetTimer();
            }
        }

        bool IsSpawns()
        {
            foreach(Spawner spawner in _spawners)
            {
                if (spawner.IsSpawn == true) return true;
            }
            return false;
        } 
        #endregion
        #region public methods
        public void AddSpawner( Spawner spawner_)
        {
            if (_countWaves < spawner_.CountWaves)
            {
                _countWaves = spawner_.CountWaves;
            }
            Test2.Instace().Main.Waves = _countWaves;
            _spawners.Add(spawner_);
        }
        public void Init()
        {
            _delayBetweenWaves = Instantiate(_delayBetweenWavesPrefab).GetComponent<DelayBetweenWaves>();
            _delayBetweenWaves.CountSeconds = _countSeconds;
            _delayBetweenWaves.OnTimerEnd += NextWave;
            _spawners.Clear();
            _countWaves = 0;
            _currentWave = 0;
        }
        public void NextWave()
        {
            _currentWave++;
            Test2.Instace().Main.Wave = _currentWave;
                foreach (Spawner spawner in _spawners)
            {
                spawner.InitNextWave();
            }

        }
        static public SpawnersManager Inctance()
        {
            return _ref;
        }
        #endregion
    }
}
