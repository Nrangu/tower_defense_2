﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Spawner : MonoBehaviour
    {
        #region private fields
        [SerializeField]Transform[] _path;
        [SerializeField]EnemyData[] _enemies;
        [SerializeField] GameObject _enemy;
 //       [SerializeField] GameObject _delayBetweenWavesPrefab;
 //       DelayBetweenWaves _delayBetweenWaves;
 //       [SerializeField] int _countSeconds = 10;
        [SerializeField]int[] _waves;
        int _currentWave = -1;
        bool _isSpawn = false;

        int _countEnemies = 0;
        int _currentEnemy = 0;
        [SerializeField] float _interval = 2;
        float _time;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            //_delayBetweenWaves = Instantiate(_delayBetweenWavesPrefab).GetComponent<DelayBetweenWaves>();
            //_delayBetweenWaves.CountSeconds = _countSeconds;
            //_delayBetweenWaves.OnTimerEnd += NextWave;
            //GameObject spawnersManmer = GameObject.Find("SpawnersManager");
            //spawnersManmer.GetComponent<SpawnersManager>().AddSpawner(this);
            SpawnersManager.Inctance().AddSpawner(this);

            _time = _interval;
            //_path = Test2.Instace().Main.Level.Path;
            //_waves = Test2.Instace().Main.Level.Data.Waves;
            //_enemies = Test2.Instace().Main.Level.Data.Enemies;
           // Test2.Instace().Main.Waves = _waves.Length;
           // Test2.Instace().Main.Wave = _currentWave + 1;
        }

        // Update is called once per frame
        void Update()
        {
            if (!Test2.Instace().IsPlay) return;
            if (_currentWave >= _waves.Length)
            {
                _isSpawn = false;
                return;
            };
            if (_countEnemies <= 0)
            {
                _isSpawn = false;

            }

            if (_isSpawn == false) return;

            if (_time >= _interval && _currentEnemy < _waves[_currentWave])
            {
                _time = 0;
                GameObject tmpEnemy = Instantiate(_enemy);
                Vector3 position = transform.position;
                //position.z = tmpEnemy.transform.position.z;
                tmpEnemy.transform.position = position;
                tmpEnemy.GetComponent<Enemy>().OnEnemyDestroy += EnemyDestroy;
                tmpEnemy.GetComponent<Enemy>().Init(GetRandomEnemy(), _path);
                _currentEnemy++;
                
                


            }
            
            _time+=Time.deltaTime;
        }
        EnemyData GetRandomEnemy()
        {

            return _enemies[Random.Range(0, _enemies.Length)];
        }

 //       void NextWave()
 //       {
 //           _isSpawn = true;
 //           _currentWave++;
 //           if (_currentWave < _waves.Length)
 //           {
 ////               Test2.Instace().Main.Wave = _currentWave + 1;
 //               _countEnemies = _waves[_currentWave];
 //           }
 //       }
        void EnemyDestroy()
        {
            _countEnemies--;
        }
        #endregion
        #region public methods, properties
        public bool IsSpawn
        {
            get
            {
                return _isSpawn;
            }
            set
            {
                _isSpawn = value;
            }
        }
        public void InitNextWave()
        {
            _currentWave++;
            _isSpawn = true;
            if (_currentWave < _waves.Length)
            {
 ////               Test2.Instace().Main.Wave = _currentWave + 1;
                _countEnemies = _waves[_currentWave];
                _currentEnemy = 0;
            }               
        }
        public int CountWaves
        {
            get
            {
                return _waves.Length;
            }
        }
        #endregion
    }
}