﻿
using System;
using Library;
namespace Test2 { 

    public class Test2:Game
    {
        #region public fields
        public event Action OnWin = () => { };
        #endregion
        #region private fields
        static Test2 _ref = null;
        Main _mainObject = null;
        #endregion
        #region private methods
        
        Test2()
        {
        }
        #endregion
        #region public methods
        public static Test2 Instace()
        {
            if ( _ref == null)
            {
                _ref = new Test2();
            }
            return _ref;
        }
        public void GameOver()
        {
            State = GameState.GAMEOVER;
        }
        public void Start()
        {
            State = GameState.START;
        }
        public void Play()
        {
            State = GameState.PLAY;
        }
        public bool IsPlay
        {
            get
            {
                return State == GameState.PLAY;
            }
        }
        public void Win()
        {
            State = GameState.PAUSE;
            OnWin();
        }

        #endregion
        #region public properties
        public Main Main
        {
            get
            {
                return _mainObject;
            }
            set
            {
                _mainObject = value;
            }
        }
        #endregion
    }
}

