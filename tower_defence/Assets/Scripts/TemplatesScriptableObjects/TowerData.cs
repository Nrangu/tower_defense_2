﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    [CreateAssetMenu(menuName ="Test2/TowerData", fileName ="TowerData")]
    public class TowerData : ScriptableObject
    {
        #region private fields
        [Tooltip("статическое изображение башни")]
        [SerializeField] Sprite _sprite;
        [SerializeField] GameObject _shootFire;
        [Tooltip("цена постройки")]
        [SerializeField] float _buildigPrice;
        [Tooltip("цена продажи")]
        [SerializeField] float _sellingPrice;
        [Tooltip("урон")]
        [SerializeField] float _damage;
        [Tooltip("скорострельность в секунду")]
        [SerializeField] float _shootInterval;
        [Tooltip("дальность нанесения урона")]
        [SerializeField] float _range;
        [SerializeField] GameObject _menuTemplate;
        [SerializeField] GameObject[] _items;
        #endregion
        #region public properties
        public GameObject MenuTemplate
        {
            get
            {
                return _menuTemplate;
            }
        }

        public GameObject[] Items
        {
            get
            {
                return _items;
            }
        }
        public Sprite Sprite
        {
            get
            {
                return _sprite;
            }
        }
        public float BuildingPrice
        {
            get
            {
                return _buildigPrice;
            }
        }
         public float SellingPrice
        {
            get
            {
                return _sellingPrice;
            }
        }
        public float Damage
        {
            get
            {
                return _damage;
            }
        }
        public float ShootInterval
        {
            get
            {
                return _shootInterval;
            }
        }
        public float Range
        {
            get
            {
                return _range;
            }
        }
        public GameObject ShootFire
        {
            get
            {
                return _shootFire;
            }
        }
        #endregion
    }
}
