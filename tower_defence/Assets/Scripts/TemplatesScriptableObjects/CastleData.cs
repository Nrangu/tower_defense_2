﻿using UnityEngine;

namespace Test2
{

    [CreateAssetMenu(menuName ="Test2/CastleData", fileName ="CastleData")]
    public class CastleData : ScriptableObject
    {
        #region private fields
        [Tooltip("Количество жизнь у замка")]
        [SerializeField]float _health;
        [Tooltip("Изображение замка")]
        [SerializeField]Sprite _sprite;
        #endregion
        #region public methods, properties
        public float Health
        {
            get
            {
                return _health;
            }
        }

        public Sprite Sprite
        {
            get
            {
                return _sprite;
            }
        }
        #endregion
    }
}