﻿
using UnityEngine;

namespace Test2
{
    [CreateAssetMenu(menuName ="Test2/EnemyData", fileName ="EnemyData")]
    public class EnemyData : ScriptableObject
    {
        #region private fields
        [Tooltip("Статическое изображение врага")]
        [SerializeField]Sprite _sprite;

        [Tooltip("спрайт взрыва врага")]
        [SerializeField] GameObject _explosion;

        [Tooltip("колличество здоровья врага")]
        [SerializeField] float _healt;

        [Tooltip("скорость врага")]
        [SerializeField] float _speed;

        [Tooltip("урон врага")]
        [SerializeField] float _damage;

        [Tooltip("мин и макс значения получаемых монет")]
        [SerializeField]int _minCoins;
        [SerializeField]int _maxCoins;
        #endregion
        #region public methods, properties
        public float Health
        {
            get
            {
                return _healt;
            }
        }

        public float Speed
        {
            get
            {
                return _speed;
            }
        }

        public float Damage
        {
            get
            {
                return _damage;
            }
        }

        public int MinCoins
        {
            get
            {
                return _minCoins;
            }
        }

        public int MaxCoins
        {
            get
            {
                return _minCoins;
            }
        }
        public Sprite Sprite
        {
            get
            {
                return _sprite;
            }
        }
        public GameObject Explosion
        {
            get
            {
                return _explosion;
            }
        }
        #endregion
    }
}