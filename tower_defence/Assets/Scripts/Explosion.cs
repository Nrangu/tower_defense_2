﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Explosion : MonoBehaviour
    {
        #region private fields
        float _timeLive = 0f;
        float _timeDead = 0.3f;
        #endregion
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if ( _timeLive >= _timeDead)
            {
                Destroy(gameObject);
            }
            _timeLive += Time.deltaTime;
        }
    }
}