﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Library;

namespace Test2
{
    public class Tower : MonoBehaviour
    {
        #region private fields
        [SerializeField] TowerData _data;
        [SerializeField] MenuTower2 _menu;

        [SerializeField] GameObject _rotatedObject;
        [SerializeField] SpriteRenderer _spriteRenderer;
        [SerializeField] Transform _shootFirePosition;
        GameObject _shootFire;
        List<GameObject> _enemies = new List<GameObject>();
        float _time;
        Enemy _enemy = null;
        #endregion
        #region private methods
// Start is called before the first frame update
        void Start()
        {
            if (_data != null)
            {
                Init(_data);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!Test2.Instace().IsPlay) return;
            if (_enemies.Count > 0)
            {
                if (_enemy != null)
                {
//                    _rotatedObject.transform.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(transform.position, transform.up, _enemy.transform.position));
                    Rotate(_rotatedObject.transform);
                }
                Shoot();
            }
            
        }
        private void Shoot()
        {
            if (_time >= _data.ShootInterval)
            {
                _time = 0;
                 _enemy = GetEnemy();
                if (_enemy)
                {
                    //_rotatedObject.transform.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(transform.position, transform.up, _enemy.transform.position));
                    Rotate(_rotatedObject.transform);
                    StartCoroutine("ShootFire");
                    _enemy.Damage(_data.Damage);
                }
            }
            _time += Time.deltaTime;
        }
        private Enemy GetEnemy()
        {
            Transform _castlePosition = Test2.Instace().Main.CastlePosition();
            GameObject enemy = _enemies[0];
            float dist = Vector2.Distance(_castlePosition.position, enemy.transform.position);
            foreach(GameObject obj in _enemies)
            {
                float tmpDist = Vector2.Distance(_castlePosition.position, obj.transform.position);
                if (tmpDist < dist)
                {
                    dist = tmpDist;
                    enemy = obj;
                }
            }
            return enemy.GetComponent<Enemy>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject  tmp = collision.gameObject;
            if (tmp.tag == "Enemy")
            {
                _enemies.Add(tmp);
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            GameObject tmp = collision.gameObject;
            if (tmp.tag == "Enemy")
            {
                _enemies.Remove(tmp);
            }
        }
        private void Rotate( Transform _object)
        {
            _object.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(_object.position, _object.up, _enemy.transform.position));
        }
        private IEnumerator ShootFire()
        {
            GameObject tmp = Instantiate(_shootFire, _shootFirePosition);
            tmp.transform.position = _shootFirePosition.position;
            tmp.transform.rotation = _shootFirePosition.rotation;
            yield return new WaitForSeconds(0.05f);
            Destroy(tmp);
        }
        #endregion
        #region public methods, properties
        public void Init(TowerData data_)
        {
            _data = data_;
            _spriteRenderer.sprite = _data.Sprite;
            GetComponent<CircleCollider2D>().radius = _data.Range;
            _time = _data.ShootInterval;
            _shootFire = _data.ShootFire;
            _menu.MenuTemplate = _data.MenuTemplate;
            _menu.Items = _data.Items;
        }
        public TowerData Data
        {
            get
            {
                return _data;
            }
        }
        #endregion
    }
}
