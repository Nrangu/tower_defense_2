﻿using System;
using System.Collections;
using UnityEngine;

using UnityEngine.UI;
using Library;

namespace Test2
{
    public class Enemy : MonoBehaviour
    {
        #region private fields
        [SerializeField] EnemyData _data;
        [SerializeField] GameObject _rotatedObject;
        [SerializeField] SpriteRenderer _spriteRenderer;
        [SerializeField] Slider _healthSlider;
        Transform _currentTarget;
        Transform[] _targets;
        int _numTarget = 0;
        float _health;
        #endregion
        #region public fields
        public event Action OnEnemyDestroy = () => { };
        #endregion

        #region private methods
        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if ( !Test2.Instace().IsPlay) return;

            Move();
        }
        private void Move()
        {
            if (_currentTarget == null) return;

            if ( Vector2.Distance( transform.position, _currentTarget.position) > 0.1)
            {
                float z = transform.position.z;
                Vector3 newPos = Vector2.MoveTowards(transform.position, _currentTarget.position, Time.deltaTime * _data.Speed);
                newPos.z = z;
                transform.position = newPos;
                
                return;
            }
            _numTarget++;
            _currentTarget = SelectTarget();
            //transform.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(transform.position, transform.up, _currentTarget.transform.position));
            Rotate(_rotatedObject.transform);
        }
        private Transform SelectTarget()
        {
            if (_numTarget < _targets.Length)
            {
                return _targets[_numTarget];
            }
            return null;
        }
        private int DropCoins()
        {
            return UnityEngine.Random.Range(_data.MinCoins, _data.MaxCoins + 1);
        }
        private void DestroyEnemy()
        {
            OnEnemyDestroy();
            GameObject tmp = Instantiate(_data.Explosion);
            tmp.transform.position = _rotatedObject.transform.position; Destroy(gameObject);

        }
 
        private void Rotate(Transform _object)
        {
            _object.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(_object.position, _object.up, _currentTarget.transform.position));
        }
        private float Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                _healthSlider.value = _health;
            }
        }
        #endregion
        #region public methods

        public void Init(EnemyData data_, Transform[] targets_)
        {
            _data = data_;
            _spriteRenderer.sprite = _data.Sprite;
            _numTarget = 0;
            _targets = targets_;
            _currentTarget = SelectTarget();
            //transform.rotation *= Quaternion.Euler(0, 0, MatchBase.angleLookRotation(transform.position, transform.up, _currentTarget.transform.position));
            Rotate(_rotatedObject.transform);
            _healthSlider.minValue = 0;
            _healthSlider.maxValue = _data.Health;
            Health = _data.Health;
        }

        public void Damage( float damage_)
        {
            Health -= damage_;
            if (Health > 0) return;

            Dead();
        }
        public void Dead()
        {
           Test2.Instace().Main.ChangeCoins( DropCoins());
            DestroyEnemy();
        }
        public float Kamikadze()
        {
            DestroyEnemy();
            return _data.Damage;
        }
        #endregion
    }
}
