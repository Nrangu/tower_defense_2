﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class MenuTower : MonoBehaviour
    {
        #region private fields
        bool _isCanClose = false;
        GameObject _parent;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && _isCanClose)
            {
                Destroy(gameObject);
            }
            if (Input.GetMouseButtonDown(1) && _isCanClose)
            {
                Destroy(gameObject);
            }
            if (Input.GetMouseButtonDown(2) && _isCanClose)
            {
                Destroy(gameObject);
            }
            
        }
        private void OnMouseEnter()
        {
            _isCanClose = false;

        }
        private void OnMouseExit()
        {
            _isCanClose = true;



        }
        #endregion
        #region public methods, properties
        public bool CanClose
        {
            get
            {
                return _isCanClose;
            }
            set
            {
                _isCanClose = value;
            }
        }
        public GameObject Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }
        public void DestroyParent()
        {
            Destroy(_parent);
        }
        #endregion

    }
}