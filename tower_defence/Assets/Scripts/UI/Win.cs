﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Test2
{

    public class Win : MonoBehaviour, IPointerClickHandler
    {
        #region public fields
        public event Action OnWinClick = ()=> { };
        #endregion
        #region private methods
        void Start()
        {
            GameObject mainCanvas = GameObject.Find("MainCanvas");

            //transform.parent = mainCanvas.transform;
            RectTransform rect = GetComponent<RectTransform>();

            rect.parent = mainCanvas.GetComponent<RectTransform>();
            rect.localScale = new Vector3(5, 5, 1);
            rect.localPosition = new Vector3(0, 0, -10);
        }
        #endregion
        #region public methods
        public void OnPointerClick(PointerEventData ped)
        {
            OnWinClick();
        }
        #endregion
    }
}