﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Test2
{


    public class DelayBetweenWaves : MonoBehaviour
    {
        #region private fields
        [SerializeField] Text _timerText;
        int _countSeconds;
        float _timer;
        #endregion
        #region public fields
        public event Action OnTimerEnd = () => { };
        #endregion
        // Start is called before the first frame update
        #region private methods
        void Start()
        {
            transform.parent = GameObject.Find("MainCanvas").transform;
            transform.position = transform.parent.transform.position;
            transform.localScale = new Vector3(1, 1, 1);
            _timer = _countSeconds;
        }

        // Update is called once per frame
        void Update()
        {
            _timerText.text = ((int)_timer).ToString();
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                OnTimerEnd();
                gameObject.SetActive(false);

            }
        }
        #endregion
        #region public methods, properties
        public int CountSeconds
        {
            get
            {
                return _countSeconds;
            }
            set
            {
                _countSeconds = value;
            }
        }
        public bool Active
        {
            get
            {
                return gameObject.activeSelf;            }
        }
        public void ResetTimer()
        {
            _timer = CountSeconds;
            gameObject.SetActive(true);
        }
        #endregion
    }
}