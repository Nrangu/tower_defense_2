﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class MenuTower2 : MonoBehaviour
    {
        #region private fields
        //объект который создал меню
        // Он нужен что бы при постройке башни его корректно удалить
        GameObject _parent;
        [SerializeField] GameObject _menuTemplate;
        [SerializeField] GameObject[] _items;
        #endregion
        #region private methods
        private void Start()
        {
            _parent = gameObject;
        }
        #endregion
        #region public methods, properties
        public GameObject MenuTemplate
        {
            get
            {
                return _menuTemplate;
            }
            set
            {
                _menuTemplate = value;
            }
        }

        public GameObject[] Items
        {
            get
            {
                return _items;
            }

            set
            {
                _items = value;
            }
        }
        public void Init(GameObject parent_)
        {
            _parent = parent_;

                Init();
        }
        public void Init()
        {
            GameObject tmp = Instantiate(_menuTemplate);
            tmp.GetComponent<MenuTemplate>().Init(_items);
            tmp.GetComponent<MenuTemplate>().Parent = _parent;
        }

        #endregion
    }

}