﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class SellTower : MonoBehaviour
    {
        #region private fields
         Transform _parent;
        [SerializeField] GameObject _emptySpace;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _parent = gameObject.transform.parent;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
        #region public methods
        public void SellClick()
        {
            Vector3 position = _parent.GetComponent<MenuTemplate>().Parent.transform.position;
            GameObject tmp = Instantiate(_emptySpace);
            position.z = tmp.transform.position.z;
            tmp.transform.position = position;
            Test2.Instace().Main.ChangeCoins((int)_parent.GetComponent<MenuTemplate>().Parent.GetComponent<Tower>().Data.SellingPrice);
            _parent.GetComponent<MenuTemplate>().BuyingSellingTower();

        }
        #endregion
    }
}