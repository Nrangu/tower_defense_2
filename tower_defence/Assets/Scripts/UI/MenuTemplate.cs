﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Test2
{

    public class MenuTemplate : MonoBehaviour
    {
        #region private fields
        [SerializeField] RectTransform[] _itemsPos;
        GameObject _parent;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            GameObject mainCanvas = GameObject.Find("MainCanvas");

            //transform.parent = mainCanvas.transform;
            RectTransform rect = GetComponent<RectTransform>();

            rect.parent = mainCanvas.GetComponent<RectTransform>();
            rect.localScale = new Vector3(1, 1, 1);
            rect.localPosition = new Vector3(0, 0, 0);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
        #region public methods
        public GameObject Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
                transform.parent = _parent.transform;
            }
        }
 
        public void Init( GameObject[] items_)
        {

            int countItems = _itemsPos.Length;
            if (countItems > items_.Length)
            {
                countItems = items_.Length;
            }
            
            for(int i = 0; i < countItems; i++)
            {
                if (items_[i] == null) continue;
                GameObject tmpItem = Instantiate(items_[i], transform);
                
                RectTransform tmp = tmpItem.GetComponent<RectTransform>();
                tmp.localScale = new Vector3(1, 1, 1);
                tmp.localPosition = _itemsPos[i].localPosition;
 

            }

        }
        public void MenuTowerClick()
        {
            Destroy(gameObject);
        }
        public void BuyingSellingTower()
        {
            Destroy(_parent);
            Destroy(gameObject);
        }
        #endregion
    }
}