﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Test2
{

    public class BuyTower : MonoBehaviour
    {
        #region private fields
        [Tooltip("Параметры башни котороя будет построена")]
        [SerializeField] BuyTowerData _data;
        //[Tooltip("Объект содержащий все элементы меню")]
        //[SerializeField] GameObject _parent;
        //SpriteRenderer _spriteRenderer;
        Image _spriteRenderer;
        bool isEnable = true;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            //_spriteRenderer = GetComponent<SpriteRenderer>();
            _spriteRenderer = GetComponent<Button>().image;
               
            if (Test2.Instace().Main.Coins < _data.TowerData.BuildingPrice)
            {
                _spriteRenderer.sprite = _data.DisableSprite;
                isEnable = false;
            }
            else
            {
                _spriteRenderer.sprite = _data.Sprite;
                isEnable = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if ( (Test2.Instace().Main.Coins) < _data.TowerData.BuildingPrice && (isEnable))
            {
                _spriteRenderer.sprite = _data.DisableSprite;
                isEnable = false;
            }

            if ((Test2.Instace().Main.Coins) > _data.TowerData.BuildingPrice && (!isEnable))
            {
                _spriteRenderer.sprite = _data.Sprite;
                isEnable = true;
            }
        }
        #endregion
        #region public methods
        public void Click()
        {
            if (!isEnable)
            {
                return;

            };

            GameObject tmpTower = Instantiate(_data.TowerPrefab);
            float z = tmpTower.transform.position.z;
            tmpTower.GetComponent<Tower>().Init(_data.TowerData);

            Transform tmpTransform = gameObject.transform.parent.GetComponent<MenuTemplate>().Parent.transform;
            tmpTower.transform.position = new Vector3(tmpTransform.position.x, tmpTransform.position.y, z);

            Test2.Instace().Main.ChangeCoins(-(int)_data.TowerData.BuildingPrice);
            gameObject.transform.parent.GetComponent<MenuTemplate>().BuyingSellingTower();
            //Destroy(_parent);
            //Destroy(gameObject);
        }

        #endregion
    }
}